.. simple-pe documentation master file, created by
   sphinx-quickstart on Thu Jan  6 16:08:03 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to simple-pe's documentation!
=====================================

**simple-pe** is a Python library that performs simple parameter estimation for gravitational wave signals emitted by coalescing binaries.

.. note::

   This project is under active development


.. toctree::
   cosmology
   detectors
   fstat
   likelihood
   localize
   param_est
   waveforms
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
