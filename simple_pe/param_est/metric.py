import numpy as np
import lal
from lalsimulation import (
    SimInspiralFD, SimInspiralCreateModeArray, SimInspiralModeArrayActivateMode,
    SimInspiralWaveformParamsInsertModeArray, GetApproximantFromString
)
from pycbc.types import FrequencySeries
from pycbc.filter import match
from simple_pe.waveforms import waveform_modes
import copy
from scipy import optimize
from scipy.stats import chi2
from simple_pe.param_est.pe import SimplePESamples, param_maxs, param_mins
from pesummary.gw import conversions
from pesummary.utils.samples_dict import SamplesDict


class Metric:
    """
    A class to store the parameter space metric at a point x in parameter space,
    where the variations are given by dx_directions

    :param x: dictionary with parameter values for initial point
    :param dx_directions: a list of directions to vary
    :param f_low: low frequency cutoff
    :param psd: the power spectrum to use in calculating the match
    :param approximant: the approximant generator to use
    :return gij: a square matrix, with size given by the length of dxs, that gives the
    metric at x along the directions given by dxs
    """

    def __init__(self, x, dx_directions, mismatch, f_low, psd,
                 approximant="IMRPhenomD", tolerance=1e-2, prob=None, snr=None):
        """
        :param x: dictionary with parameter values for initial point
        :param dx_directions: a list of directions to vary
        :param mismatch: the mismatch value to use when calculating metric
        :param f_low: low frequency cutoff
        :param psd: the power spectrum to use in calculating the match
        :param approximant: the approximant generator to use
        :param tolerance: tolerance for scaling vectors
        :param prob: probability to enclose within ellipse
        :param snr: snr of signal, used in scaling mismatch
        """
        self.x = SimplePESamples(x)
        if 'distance' not in self.x:
            self.x['distance'] = np.ones_like(list(x.values())[0])
        self.dx_directions = dx_directions
        self.ndim = len(dx_directions)
        self.dxs = SimplePESamples(SamplesDict(self.dx_directions, np.eye(self.ndim)))
        self.mismatch = mismatch
        self.f_low = f_low
        self.psd = psd
        self.approximant = approximant
        self.tolerance = tolerance
        self.prob = prob
        self.snr = snr
        if self.prob:
            self.n_sigma = np.sqrt(chi2.isf(1 - self.prob, self.ndim))
            if self.snr:
                self.mismatch = self.n_sigma ** 2 / (2 * self.snr ** 2)
        self.coordinate_metric = None
        self.metric = None
        self.evals = None
        self.evec = None
        self.err = None
        self.projected_directions = None
        self.projected_metric = None
        self.projection = None
        self.projected_mismatch = None

        self.scale_dxs()
        self.calculate_metric()

    def scale_dxs(self):
        """
        This function scales the vectors dxs so that the mismatch between
        a waveform at point x and one at x + dxs[i] is equal to the specified
        mismatch, up to the specified tolerance.
        """
        scale = np.zeros(self.ndim)
        for i in range(self.ndim):
            dx = self.dxs[i:i + 1]
            scale[i] = scale_dx(self.x, dx, self.mismatch, self.f_low, self.psd,
                                self.approximant, self.tolerance)

        self.dxs = SimplePESamples(SamplesDict(self.dx_directions, self.dxs.samples * scale))

    def calculate_metric(self):
        scaling = 1.
        gij = np.zeros([self.ndim, self.ndim])

        # diagonal components
        # g_ii = 1 - 0.5 [m(dx_i) + m(-dx_i)]
        for i in range(self.ndim):
            gij[i, i] += average_mismatch(self.x, self.dxs[i:i + 1], scaling, self.f_low,
                                          self.psd, self.approximant)

        # off diagonal
        # g_ij = 0.25 * [+ m(1/sqrt(2) (+ dx_i + dx_j)) + m(1/sqrt(2) (- dx_i - dx_j))
        #                - m(1/sqrt(2) (+ dx_i - dx_j)) - m(1/sqrt(2) (- dx_i + dx_j))]
        for i in range(self.ndim):
            for j in range(i + 1, self.ndim):
                for s in ([1, -1]):
                    dx = {}
                    for k, vals in self.dxs.items():
                        dx[k] = (vals[i] + s * vals[j]) / np.sqrt(2)
                    gij[i, j] += 0.5 * s * average_mismatch(self.x, dx, scaling,
                                                            self.f_low, self.psd, self.approximant)
                gij[j, i] = gij[i, j]

        # this gives the metric in coordinates given by the dxs.
        # Let's instead return the metric in physical parameter space
        self.coordinate_metric = gij
        self.physical_metric()

    def physical_metric(self):
        """
        A function to calculate the metric in physical coordinates
        """
        dx_inv = np.linalg.inv(self.dxs.samples)
        self.metric = np.matmul(dx_inv.T, np.matmul(self.coordinate_metric, dx_inv))

    def calculate_evecs(self):
        """
        A function to calculate the eigenvectors and eigenvalues of the metric gij
        """
        self.evals, self.evec = np.linalg.eig(self.metric)

    def normalized_evecs(self):
        """
        Return the evecs normalized to give the desired mismatch
        """
        if self.evec is None:
            self.calculate_evecs()
        # always force to be positive to avoid negatives in sqrt
        self.evals[self.evals < 0] = np.abs(self.evals[self.evals < 0])
        return SimplePESamples(SamplesDict(self.dx_directions, self.evec * np.sqrt(self.mismatch / self.evals)))

    def calc_metric_error(self):
        """
        We are looking for a metric and corresponding basis for which the
        basis is orthogonal and whose normalization is given by the desired mismatch
        This function checks for the largest error

        """
        vgv = np.matmul(self.dxs.samples.T, np.matmul(self.metric, self.dxs.samples))
        off_diag = np.max(abs(vgv[~np.eye(self.metric.shape[0], dtype=bool)]))
        diag = np.max(abs(np.diag(vgv)) - self.mismatch)
        self.err = max(off_diag, diag)

    def update_metric(self):
        """
        A function to re-calculate the metric gij based on the matches obtained
        for the eigenvectors of the original metric
        """
        # calculate the eigendirections of the matrix
        self.calculate_evecs()
        self.dxs = SimplePESamples(SamplesDict(self.dx_directions, self.evec))
        # scale so that they actually have the desired mismatch
        self.scale_dxs()
        self.calculate_metric()

    def iteratively_update_metric(self, max_iter=20):
        """
        A method to re-calculate the metric gij based on the matches obtained
        for the eigenvectors of the original metric
        """
        tol = self.tolerance * self.mismatch
        self.calc_metric_error()

        op = 0

        while (self.err > tol) and (op < max_iter):
            self.update_metric()
            self.calc_metric_error()
            op += 1

        if self.err > tol:
            print("Failed to achieve requested tolerance.  Requested: %.2g; achieved %.2g" % (tol, self.err))

    def project_metric(self, projected_directions):
        """
        Project out the unwanted directions of the metric

        :param projected_directions: list of parameters that we want to keep
        """
        self.projected_directions = projected_directions
        kept = np.ones(self.ndim, dtype=bool)
        for i, x in enumerate(self.dx_directions):
            if x not in projected_directions:
                kept[i] = False

        # project out unwanted: g'_ab = g_ab - g_ai (ginv)^ij g_jb (where a,b run over kept, i,j over removed)
        # matrix to calculate the values of the maximized directions: x^i = (ginv)^ij g_jb
        ginv = np.linalg.inv(self.metric[~kept][:, ~kept])
        self.projection = - np.matmul(ginv, self.metric[~kept][:, kept])
        self.projected_metric = self.metric[kept][:, kept] + np.matmul(self.metric[kept][:, ~kept], self.projection)

        if self.prob and self.snr:
            # calculate equivalent mismatch given number of remaining dimensions
            n_sigmasq = chi2.isf(1 - self.prob, len(projected_directions))
            self.projected_mismatch = n_sigmasq / (2 * self.snr ** 2)

    def projected_evecs(self):
        """
        Return the evecs normalized to give the desired mismatch
        """
        evals, evec = np.linalg.eig(self.projected_metric)
        return SimplePESamples(SamplesDict(self.projected_directions, evec * np.sqrt(self.mismatch / evals)))

    def generate_ellipse(self, npts=100, projected=False, scale=1.):
        """
        Generate an ellipse of points of the stored mismatch.  Scale the radius by a factor `scale'
        If the metric is projected, and we know the snr, then scale the mismatch appropriately for the number
        of projected dimensions.

        :param npts: number of points in the ellipse
        :param projected: use the projected metric if True, else use metric
        :param scale: scaling to apply to the mismatch
        :return ellipse_dict: SimplePESamples with ellipse of points
        """
        if projected:
            dx_dirs = self.projected_directions
            n_evec = self.projected_evecs()
        else:
            dx_dirs = self.dx_directions
            n_evec = self.normalized_evecs()

        if len(dx_dirs) != 2:
            print("We're expecting to plot a 2-d ellipse")
            return -1

        if projected and self.projected_mismatch:
            scale *= np.sqrt(self.projected_mismatch / self.mismatch)

        # generate points on a circle
        phi = np.linspace(0, 2 * np.pi, npts)
        xx = scale * np.cos(phi)
        yy = scale * np.sin(phi)
        pts = np.array([xx, yy])

        # project onto eigendirections
        dx = SimplePESamples(SamplesDict(dx_dirs, np.matmul(n_evec.samples, pts)))

        # scale to be physical
        alphas = np.zeros(npts)
        for i in range(npts):
            alphas[i] = check_physical(self.x, dx[i:i + 1], 1.)

        ellipse_dict = SimplePESamples(SamplesDict(dx_dirs,
                                                   np.array([self.x[dx] for dx in dx_dirs]).reshape(
                                                       [2, 1]) + dx.samples * alphas))

        return ellipse_dict

    def generate_match_grid(self, npts=10, projected=False, scale=1.):
        """
        Generate a grid of points with extent governed by stored mismatch.  
        If the metric is projected, scale to the projected number of dimensions
        Apply an overall scaling of scale

        :param npts: number of points in each dimension of the grid
        :param projected: use the projected metric if True, else use metric
        :param scale: a factor by which to scale the extent of the grid
        :return ellipse_dict: SimplePESamples with grid of points and match at each point
        """
        if projected:
            dx_dirs = self.projected_directions + [x for x in self.dx_directions if x not in self.projected_directions]
            n_evec = self.projected_evecs()
        else:
            dx_dirs = self.dx_directions
            n_evec = self.normalized_evecs()

        if projected and self.projected_mismatch:
            scale *= np.sqrt(self.projected_mismatch / self.mismatch)

        grid = np.mgrid[-scale:scale:npts * 1j, -scale:scale:npts * 1j]
        dx_data = np.tensordot(n_evec.samples, grid, axes=(1, 0))

        if projected:
            dx_extra = np.tensordot(self.projection, dx_data, axes=(1, 0))
            dx_data = np.append(dx_data, dx_extra, 0)

        dx = SimplePESamples(SamplesDict(dx_dirs, dx_data.reshape(len(dx_dirs), npts ** 2)))

        h0 = make_waveform(self.x, self.psd.delta_f, self.f_low, len(self.psd), self.approximant)

        m = np.zeros(dx.number_of_samples)
        for i in range(dx.number_of_samples):
            if check_physical(self.x, dx[i:i + 1], 1.) < 1:
                m[i] = 0
            else:
                h1 = make_offset_waveform(self.x, dx[i:i + 1], 1.,
                                          self.psd.delta_f, self.f_low, len(self.psd), self.approximant)
                m[i] = match(h0, h1, self.psd, self.f_low, subsample_interpolation=True)[0]

        matches = SimplePESamples(SamplesDict(dx_dirs + ['match'],
                                              np.append(
                                                  dx.samples + np.array([self.x[dx] for dx in dx_dirs]).reshape(
                                                      [len(dx_dirs), 1]),
                                                  m.reshape([1, npts ** 2]), 0).reshape(
                                                  [len(dx_dirs) + 1, npts, npts])))

        return matches

    def generate_posterior_grid(self, npts=10, projected=False, scale=None):
        """
        Generate a grid of points with extent governed by requested mismatch

        :param npts: number of points in each dimension of the grid
        :param projected: use the projected metric if True, else use metric
        :param scale: the scale to apply to the greatest extent of the grid
        :return ellipse_dict: SimplePESamples with grid of points and match at each point
        """
        if not self.snr:
            print("need an SNR to turn matches into probabilities")
            return -1

        matches = self.generate_match_grid(npts, projected, scale)
        post = np.exp(-self.snr ** 2 / 2 * (1 - matches['match'] ** 2))

        grid_probs = SimplePESamples(SamplesDict(matches.keys() + ['posterior'],
                                                 np.append(matches.samples, post).reshape(
                                                     [len(matches.keys()) + 1, npts, npts])))

        return grid_probs

    def generate_samples(self, npts=int(1e5)):
        """
        Generate an ellipse of points of constant mismatch

        :param npts: number of points to generate
        :return phys_samples: SimplePESamples with samples
        """
        pts = np.random.normal(0, 1, [2 * npts, self.ndim])

        sample_pts = SimplePESamples(SamplesDict(self.dx_directions,
                                                 (np.array([self.x[dx] for dx in self.normalized_evecs().keys()
                                                            ]).reshape([len(self.dx_directions), 1])
                                                  + np.matmul(pts,
                                                              self.normalized_evecs().samples.T / self.n_sigma).T)))
        sample_pts.trim_unphysical()
        if sample_pts.number_of_samples > npts:
            sample_pts = sample_pts.downsample(npts)

        return sample_pts


def make_waveform(params, df, f_low, flen, approximant="IMRPhenomD", return_hc=False, modes=None, harm2=False):
    """
    This function makes a waveform for the given parameters and
    returns h_plus generated at value x.

    :param params: SimplePESamples with parameter values for waveform generation
    :param df: frequency spacing of points
    :param f_low: low frequency cutoff
    :param flen: length of the frequency domain array to generate
    :param approximant: the approximant generator to use
    :param return_hc: flag to choose to return cross polarization (only non-precessing)
    :param modes: the modes to generate (only for non-precessing)
    :param harm2: generate the 2-harmonics
    :return h_plus: waveform at parameter space point x
    """
    for k, i in params.items():
        if not hasattr(i, '__len__'):
            params[k] = [i]

    x = SimplePESamples(params)
    if 'phase' not in x.keys():
        x['phase'] = np.zeros_like(list(x.values())[0])
    if 'f_ref' not in x.keys():
        x['f_ref'] = f_low * np.ones_like(list(x.values())[0])

    if modes is None:
        modes = waveform_modes.mode_array('22', approximant)

    if 'chi' in x.keys() and 'tilt' in x.keys():
        x['chi_p'] = x['chi'] * np.sin(x['tilt'])
        x['chi_align'] = x['chi'] * np.cos(x['tilt'])
        x.pop('tilt')
        x.pop('chi')

    prec = "chi_p" if "chi_p" in x.keys() else "chi_p2"

    if (prec in x.keys()) and x[prec]:
        # generate the leading harmonic of the precessing waveform
        x.generate_prec_spin()

    if 'tilt_1' in x.keys() and x['tilt_1']:
        x.generate_all_posterior_samples(f_low=f_low, f_ref=x["f_ref"][0], delta_f=df, disable_remnant=True)
        if harm2:
            harmonics = [0, 1]
        else:
            harmonics = [0]

        h_plus = conversions.snr._calculate_precessing_harmonics(x["mass_1"][0], x["mass_2"][0],
                                                                 x["a_1"][0], x["a_2"][0],
                                                                 x["tilt_1"][0], x["tilt_2"][0],
                                                                 x["phi_12"][0],
                                                                 x["beta"][0], x["distance"][0],
                                                                 harmonics=harmonics, approx=approximant,
                                                                 mode_array=modes,
                                                                 df=df, f_low=f_low,
                                                                 f_ref=x["f_ref"][0])
        if return_hc:
            print('return_hc not available for precessing system')
        for k, h in h_plus.items():
            h.resize(flen)
        if not harm2:
            return h_plus[0]
        return h_plus

    else:
        if harm2:
            raise ValueError(
                "Currently unable to calculate 2 harmonic decomposition when "
                "lalsimulation.SimInspiralFD is called"
            )
        if ('spin_1z' not in x.keys()) or ('spin_2z' not in x.keys()):
            x.generate_spin_z()
        if "inc" not in x.keys():
            x["inc"] = 0.

        x.generate_all_posterior_samples(f_low=f_low, f_ref=x["f_ref"][0], delta_f=df, disable_remnant=True)
        waveform_dictionary = lal.CreateDict()
        mode_array_lal = SimInspiralCreateModeArray()
        for mode in modes:
            SimInspiralModeArrayActivateMode(mode_array_lal, mode[0], mode[1])
        SimInspiralWaveformParamsInsertModeArray(waveform_dictionary, mode_array_lal)
        if isinstance(x["mass_1"], list):
            m1 = x["mass_1"][0]
        else:
            m1 = x["mass_1"]
        if isinstance(x["mass_2"], list):
            m2 = x["mass_2"][0]
        else:
            m2 = x["mass_2"]
        if "eccentricity" in x.keys():
            if isinstance(x["eccentricity"], list):
                ecc = x["eccentricity"][0]
            else:
                ecc = x["eccentricity"]
        elif "ecc2" in x.keys():
            if isinstance(x["ecc2"], list):
                ecc = x["ecc2"][0]**0.5
            else:
                ecc = x["ecc2"]**0.5  
        else:
            ecc = 0.
            
        args = [
            m1 * lal.MSUN_SI, m2 * lal.MSUN_SI, 0., 0.,
            x["spin_1z"], 0., 0., x["spin_2z"], x['distance'] * 1e6 * lal.PC_SI,
            x["inc"], 0., 0., ecc, 0., df, f_low, 2048., x['f_ref']
        ]
        args = [float(arg) for arg in args]
        hp, hc = SimInspiralFD(
            *args, waveform_dictionary, GetApproximantFromString(approximant)
        )
        dt = 1 / hp.deltaF + (hp.epoch.gpsSeconds + hp.epoch.gpsNanoSeconds * 1e-9)
        time_shift = np.exp(
            -1j * 2 * np.pi * dt * np.array(range(len(hp.data.data[:]))) * hp.deltaF
        )
        hp.data.data[:] *= time_shift
        hc.data.data[:] *= time_shift
        h_plus = FrequencySeries(hp.data.data[:], delta_f=hp.deltaF, epoch=hp.epoch)
        h_cross = FrequencySeries(hc.data.data[:], delta_f=hc.deltaF, epoch=hc.epoch)

    h_plus.resize(flen)
    h_cross.resize(flen)
    if return_hc:
        return h_plus, h_cross
    return h_plus


def offset_params(x, dx, scaling):
    """
    Update the parameters x by moving to a value (x + scaling * dx)

    :param x: dictionary with parameter values for initial point
    :param dx: dictionary with parameter variations (can be a subset of the parameters in x)
    :param scaling: the scaling to apply to dx
    :return x_prime: parameter space point x + scaling * dx
    """
    x_prime = copy.deepcopy(x)

    for k, dx_val in dx.items():
        if k not in x_prime:
            print("Value for %s not given at initial point" % k)
            return -1

        x_prime[k] += float(scaling * dx_val)

    return x_prime


def make_offset_waveform(x, dx, scaling, df, f_low, flen, approximant="IMRPhenomD"):
    """
    This function makes a waveform for the given parameters and
    returns h_plus generated at value (x + scaling * dx).

    :param x: dictionary with parameter values for initial point
    :param dx: dictionary with parameter variations (can be a subset of the parameters in x)
    :param scaling: the scaling to apply to dx
    :param df: frequency spacing of points
    :param f_low: low frequency cutoff
    :param flen: length of the frequency domain array to generate
    :param approximant: the approximant generator to use
    :return h_plus: waveform at parameter space point x + scaling * dx
    """
    h_plus = make_waveform(offset_params(x, dx, scaling), df, f_low, flen, approximant)

    return h_plus


def check_physical(x, dx, scaling, maxs=None, mins=None, verbose=False):
    """
    A function to check whether the point described by the positions x + dx is
    physically permitted.  If not, rescale and return the scaling factor

    :param x: dictionary with parameter values for initial point
    :param dx: dictionary with parameter variations
    :param scaling: the scaling to apply to dx
    :param maxs: a dictionary with the maximum permitted values of the physical parameters
    :param mins: a dictionary with the minimum physical values of the physical parameters
    :param verbose: print logging messages
    :return alpha: the scaling factor required to make x + scaling * dx physically permissible
    """
    if mins is None:
        mins = param_mins

    if maxs is None:
        maxs = param_maxs

    x0 = offset_params(x, dx, 0.)
    if verbose:
        print('initial point')
        print(x0)
    x_prime = offset_params(x, dx, scaling)
    if verbose:
        print('proposed point')
        print(x_prime)

    alpha = 1.

    if ('chi_p' in x_prime.keys()) or ('chi_p2' in x_prime.keys()):
        if ('chi_p2' in x_prime.keys()) and (x_prime['chi_p2'] < mins['chi_p2']):
            alpha = min(alpha, (x0['chi_p2'] - mins['chi_p2']) / (x0['chi_p2'] - x_prime['chi_p2']))
            x_prime['chi_p2'][0] = mins['chi_p2']
            if verbose:
                print("scaling to %.2f in direction %s" % (alpha, 'chi_p2'))
        x_prime.generate_spin_z()
        x_prime.generate_prec_spin()
        x0.generate_spin_z()
        x0.generate_prec_spin()

    for k, dx_val in x0.items():
        if k in mins.keys() and x_prime[k] < mins[k]:
            alpha = min(alpha, (x0[k] - mins[k]) / (x0[k] - x_prime[k]))
            if verbose:
                print("scaling to %.2f in direction %s" % (alpha, k))
        if k in maxs.keys() and x_prime[k] > maxs[k]:
            alpha = min(alpha, (maxs[k] - x0[k]) / (x_prime[k] - x0[k]))
            if verbose:
                print("scaling to %.2f in direction %s" % (alpha, k))

    # if varying 'chi_p2' need to double-check we don't go over limits
    if 'chi_p2' in dx.keys() and (scaling * dx['chi_p2']):
        chia = "chi_eff" if "chi_eff" in x0.keys() else "chi_align"
        # need find alpha s.t. (chi + alpha dchi)^2 + chi_p2 + alpha dchi_p2 = max_spin^2
        c = x0[chia] ** 2 + x0['chi_p2'] - maxs['a_1']**2
        dcp2 = scaling * dx['chi_p2']
        if chia in dx.keys() and dx[chia]:
            dchi = scaling * dx[chia]
            a = dchi ** 2
            b = 2 * x0[chia] * dchi + dcp2
            alpha_prec = (-b + np.sqrt(b ** 2 - 4 * a * c)) / (2 * a)
        else:
            # not changing aligned spin, so easier
            # chi^2 + chi_p2 + alpha dchi_p2 = max_spin^2
            # if dchi_p2 < 0 then bound is positivity of chi_p2, else alpha = -c/dcp2
            if dcp2 < 0:
                # chi_p2 + alpha dchi_p2 = mins['chi_p2']
                alpha_prec = (mins['chi_p2'] - x0['chi_p2']) /dcp2
            else:
                alpha_prec = -c / dcp2
        if verbose:
            print("scaling to %.2f for precession" % alpha_prec)

        alpha = min(alpha, alpha_prec)

        if verbose:
            x_prime = offset_params(x, dx, alpha * scaling)
            print('new point')
            print(x_prime)

    return alpha


def scale_match(m_alpha, alpha):
    """
    A function to scale the match calculated at an offset alpha to the
    match at unit offset

    :param m_alpha: the match at an offset alpha
    :param alpha: the value of alpha
    :return m: the match at unit offset
    """
    m = (alpha ** 2 - 1 + m_alpha) / alpha ** 2

    return m


def average_mismatch(x, dx, scaling, f_low, psd,
                     approximant="IMRPhenomD", verbose=False):
    """
    This function calculates the average match for steps of +dx and -dx
    It also takes care of times when one of the steps moves beyond the
    edge of the physical parameter space

    :param x : dictionary with the initial point
    :param dx: dictionary with parameter variations
    :param scaling: the scaling to apply to dx
    :param f_low: low frequency cutoff
    :param psd: the power spectrum to use in calculating the match
    :param approximant: the approximant generator to use
    :param verbose: print debugging information
    :return m: The average match from steps of +/- scaling * dx
    """
    a = {}
    m = {}
    h0 = make_waveform(x, psd.delta_f, f_low, len(psd), approximant)
    for s in [1., -1.]:
        a[s] = check_physical(x, dx, s * scaling)
        try:
            h = make_offset_waveform(x, dx, s * a[s] * scaling, psd.delta_f, f_low, len(psd), approximant)
            m[s] = match(h0, h, psd, low_frequency_cutoff=f_low, subsample_interpolation=True)[0]
        except RuntimeError:
            m[s] = 1e-5

    if verbose:
        print("Had to scale steps to %.2f, %.2f" % (a[-1], a[1]))
        print("Mismatches %.3g, %.3g" % (1 - m[-1], 1 - m[1]))
    if min(a.values()) < 1e-2:
        if verbose:
            print("we're really close to the boundary, so down-weight match contribution")
        mm = (2 - m[1] - m[-1]) / (a[1] ** 2 + a[-1] ** 2)
    else:
        mm = 1 - 0.5 * (scale_match(m[1], a[1]) + scale_match(m[-1], a[-1]))
    return mm


def scale_dx(x, dx, desired_mismatch, f_low, psd,
             approximant="IMRPhenomD", tolerance=1e-2):
    """
    This function scales the input vectors so that the mismatch between
    a waveform at point x and one at x + v[i] is equal to the specified
    mismatch, up to the specified tolerance.

    :param x: dictionary with parameter values for initial point
    :param dx: a dictionary with the initial change in parameters
    :param desired_mismatch: the desired mismatch (1 - match)
    :param f_low: low frequency cutoff
    :param psd: the power spectrum to use in calculating the match
    :param approximant: the approximant generator to use
    :param tolerance: the maximum fractional error in the mismatch
    :return scale: The required scaling of dx to achieve the desired mismatch
    """
    opt = optimize.root_scalar(lambda a: average_mismatch(x, dx, a, f_low, psd,
                                                          approximant=approximant) - desired_mismatch,
                               bracket=[0, 20], method='brentq', rtol=tolerance)
    scale = opt.root

    return scale


def find_metric_and_eigendirections(x, dx_directions, snr, f_low, psd, approximant="IMRPhenomD",
                                    tolerance=0.05, max_iter=20):
    """
    Calculate the eigendirections in parameter space, normalized to enclose a 90% confidence
    region at the requested SNR

    :param x: dictionary with parameter values for initial point
    :param dx_directions: list of parameters for which to calculate waveform variations
    :param snr: the observed SNR
    :param f_low: low frequency cutoff
    :param psd: the power spectrum to use in calculating the match
    :param approximant: the approximant to use
    :param tolerance: the allowed error in the metric is (tolerance * mismatch)
    :param max_iter: the maximum number of iterations
    :return g: the mismatch metric in physical space
    """
    # initial directions and initial spacing
    g = Metric(x, dx_directions, 0, f_low, psd, approximant, tolerance,
               prob=0.9, snr=snr)
    g.iteratively_update_metric(max_iter)
    return g


def _neg_wf_match(x, x_directions, data, f_low, psd, approximant, fixed_pars=None):
    """
    Calculate the negative waveform match, taking x as the values and
    dx as the parameters.  This is in a format that's appropriate
    for scipy.optimize

    :param x: list of values
    :param x_directions: list of parameters for which to calculate waveform variations
    :param data: the data containing the waveform of interest
    :param f_low: low frequency cutoff
    :param psd: the power spectrum to use in calculating the match
    :param approximant: the approximant to use
    :param fixed_pars: a dictionary of fixed parameters and values
    :return -m: the negative of the match at this point
    """
    s = dict(zip(x_directions, x))
    if fixed_pars is not None:
        s.update(fixed_pars)

    h = make_waveform(s, psd.delta_f, f_low, len(psd), approximant)

    m = match(data, h, psd, low_frequency_cutoff=f_low, subsample_interpolation=True)[0]

    return -m


def _log_wf_mismatch(x, x_directions, data, f_low, psd, approximant, fixed_pars=None):
    """
    Calculate the negative waveform match, taking x as the values and
    dx as the parameters.  This is in a format that's appropriate
    for scipy.optimize

    :param x: list of values
    :param x_directions: list of parameters for which to calculate waveform variations
    :param data: the data containing the waveform of interest
    :param f_low: low frequency cutoff
    :param psd: the power spectrum to use in calculating the match
    :param approximant: the approximant to use
    :param fixed_pars: a dictionary of fixed parameters and values
    :return -m: the negative of the match at this point
    """
    s = dict(zip(x_directions, x))
    if fixed_pars is not None:
        s.update(fixed_pars)

    h = make_waveform(s, psd.delta_f, f_low, len(psd), approximant)

    m = match(data, h, psd, low_frequency_cutoff=f_low, subsample_interpolation=True)[0]

    return np.log10(1 - m)


def find_best_match(data, x, dx_directions, f_low, psd, approximant="IMRPhenomD",
                    method="metric", mismatch=0.03, tolerance=0.01):
    """
    A function to find the maximum match.
    This is done in two steps, first by finding the point in the grid defined
    by the metric gij (and given desired_mismatch) that gives the highest match.
    Second, we approximate the match as quadratic and find the maximum.

    :param data: the data containing the waveform of interest
    :param x: dictionary with parameter values for initial point
    :param dx_directions: list of parameters for which to calculate waveform variations
    :param f_low: low frequency cutoff
    :param psd: the power spectrum to use in calculating the match
    :param approximant: the approximant to use
    :param method: how to find the maximum
    :param mismatch: the mismatch for calculating the metric
    :param tolerance: the allowed error in the metric is (tolerance * mismatch)
    :return x_prime: the point in the grid with the highest match
    :return m_0: the match at this point
    """
    if (method != 'metric') and (method != 'scipy'):
        print('Have only implemented metric and scipy optimize based methods')
        m_peak = -1

    elif method == 'scipy':
        bounds = [(param_mins[k], param_maxs[k]) for k in dx_directions]
        x0 = np.array([x[k] for k in dx_directions])
        fixed_pars = {k: v for k, v in x.items() if k not in dx_directions}

        # out = optimize.minimize(_neg_wf_match, x0,
        #                         args=(dx_directions, data, f_low, psd, approximant, fixed_pars),
        #                         bounds=bounds)
        #
        # x = dict(zip(dx_directions, out.x))
        # m_peak = -out.fun

        out = optimize.minimize(_log_wf_mismatch, x0,
                                args=(dx_directions, data, f_low, psd, approximant, fixed_pars),
                                bounds=bounds)

        x = dict(zip(dx_directions, out.x))
        m_peak = 1 - 10 ** out.fun

    elif method == 'metric':
        g = Metric(x, dx_directions, mismatch, f_low, psd, approximant, tolerance)
        g.iteratively_update_metric()

        while True:
            h = make_waveform(x, g.psd.delta_f, g.f_low, len(g.psd), g.approximant)

            m_0, _ = match(data, h, g.psd, low_frequency_cutoff=g.f_low, subsample_interpolation=True)
            matches = np.zeros([g.ndim, 2])
            alphas = np.zeros([g.ndim, 2])

            for i in range(g.ndim):
                for j in range(2):
                    alphas[i, j] = check_physical(x, g.normalized_evecs()[i:i + 1], (-1) ** j)
                    h = make_offset_waveform(x, g.normalized_evecs()[i:i + 1], alphas[i, j] * (-1) ** j,
                                             g.psd.delta_f, g.f_low, len(g.psd),
                                             g.approximant)
                    matches[i, j] = match(data, h, g.psd, low_frequency_cutoff=g.f_low,
                                          subsample_interpolation=True)[0]

            if matches.max() > m_0:
                # maximum isn't at the centre so update location
                i, j = np.unravel_index(np.argmax(matches), matches.shape)
                for k, dx_val in g.normalized_evecs()[i:i + 1].items():
                    x[k] += float(alphas[i, j] * (-1) ** j * dx_val)

            else:
                # maximum is at the centre
                break

        s = (matches[:, 0] - matches[:, 1]) * 0.25 / \
            (m_0 - 0.5 * (matches[:, 0] + matches[:, 1]))
        delta_x = SimplePESamples(SamplesDict(dx_directions, np.matmul(g.normalized_evecs().samples, s)))
        alpha = check_physical(x, delta_x, 1)

        h = make_offset_waveform(x, delta_x, alpha, psd.delta_f, f_low, len(psd), approximant)
        m_peak = match(data, h, psd, low_frequency_cutoff=f_low,
                       subsample_interpolation=True)[0]

        for k, dx_val in delta_x.items():
            x[k] += float(alpha * dx_val)

    return x, m_peak
